# GitLab CI template for Cloud Native Buildpacks

This project implements a GitLab CI/CD template to transform your application source code into 
images that can run on any cloud with [Cloud Native Buildpacks](https://buildpacks.io/).

## Usage

In order to include this template in your project, add the following to your `gitlab-ci.yml`:

```yaml
include:
  - project: 'to-be-continuous/cnb'
    ref: '1.0.2'
    file: '/templates/gitlab-ci-cnb.yml'
```

## Understanding the CNB template

### Global configuration

The CNB template uses some global configuration used throughout all jobs.

| Name                  | description                            | default value     |
| --------------------- | -------------------------------------- | ----------------- |
| `CNB_BUILDER_IMAGE`   | The CNB builder image used to build your application image<br/>_depending on your needs, [choose the most appropriate one](https://paketo.io/docs/concepts/builders/#what-paketo-builders-are-available)_ | `registry.hub.docker.com/paketobuildpacks/builder:base` |
| `CNB_PLATFORM_API`    | The CNB [platform API version](https://github.com/buildpacks/spec/blob/main/platform.md#platform-api-version) | `0.9` |

### Available Builders

Depending on your needs and preferences, you are free to choose whichever CNB compliant builder (simply override the `CNB_BUILDER_IMAGE` variable):

| Name                  | Builder Image                    | Description     |
| --------------------- | -------------------------------- | ----------------- |
|	[Google](https://github.com/GoogleCloudPlatform/buildpacks) | `gcr.io/buildpacks/builder:v1`   | Ubuntu 18 base image with buildpacks for .NET, Go, Java, Node.js, and Python |
|	[Heroku](https://github.com/heroku/builder) | `registry.hub.docker.com/heroku/buildpacks:20`           | Base builder for Heroku-20 stack, based on ubuntu:20.04 base image |
|	[Paketo](https://hub.docker.com/r/paketobuildpacks/builder) (_base_) | `registry.hub.docker.com/paketobuildpacks/builder:base`  | Ubuntu bionic base image with buildpacks for Java, .NET Core, NodeJS, Go, Python, Ruby, Apache HTTPD, NGINX and Procfile |
|	[Paketo](https://hub.docker.com/r/paketobuildpacks/builder) (_full_) | `registry.hub.docker.com/paketobuildpacks/builder:full`  | Ubuntu bionic base image with buildpacks for Java, .NET Core, NodeJS, Go, Python, PHP, Ruby, Apache HTTPD, NGINX and Procfile |
|	[Paketo](https://hub.docker.com/r/paketobuildpacks/builder) (_tiny_) | `registry.hub.docker.com/paketobuildpacks/builder:tiny`  | Tiny base image (bionic build image, distroless-like run image) with buildpacks for Java, Java Native Image and Go |
<!--
|	[CloudFoundry](https://hub.docker.com/r/cloudfoundry/cnb) (_base_) | `cloudfoundry/cnb:base`  | A minimal image based on Ubuntu, with the addition of a few packages - notably, git and build-essentials (added in order to successfully run go applications) |
|	[CloudFoundry](https://hub.docker.com/r/cloudfoundry/cnb) (_full_) | `cloudfoundry/cnb:full`  | An image based on Ubuntu supplemented with additional packages |
|	[CloudFoundry](https://hub.docker.com/r/cloudfoundry/cnb) (_tiny_) | `cloudfoundry/cnb:tiny`  | A minimal build image (same as base), with a tiny, distroless-like run image based on Ubuntu 18.04 (Bionic Beaver) |
-->

### Images

The CNB template builds a container image that may be [pushed](https://docs.docker.com/engine/reference/commandline/push/)
as two distinct images, depending on a certain _workflow_:

1. **snapshot**: the image is first built and pushed to some container registry as
  the **snapshot** image. It can be seen as the raw result of the build, but still **untested and unreliable**.
2. **release**: once the snapshot image has been thoroughly tested (both by `package-test` stage jobs and/or `acceptance`
  stage jobs after being deployed to some server), then the image is pushed one more time as the **release** image.
  This second push can be seen as the **promotion** of the snapshot image being now **tested and reliable**.

In practice:

* the **snapshot** image is **always pushed** by the template (pipeline triggered by a Git tag or commit on any branch),
* the **release** image is only pushed:
    * on a pipeline triggered by a Git tag,
    * on a pipeline triggered by a Git commit on `master`.

The **snapshot** and **release** images are defined by the following variables:

| Name                   | Description        | Default value                                     |
| ---------------------- | ------------------ | ------------------------------------------------- |
| `CNB_SNAPSHOT_IMAGE`   | CNB snapshot image | `$CI_REGISTRY_IMAGE/snapshot:$CI_COMMIT_REF_SLUG` |
| `CNB_RELEASE_IMAGE`    | CNB release image  | `$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME`          |

As you can see, the CNB template is configured by default to use the GitLab container registry.
You may perfectly override this and use another container registry, but be aware of a few things:

* the `CNB_SNAPSHOT_IMAGE` requires a container registry that allows tag overwrite,
* the `CNB_RELEASE_IMAGE` _may_ use a container registry that doesn't allow tag overwrite, but:
    1. you should avoid overwriting a Git tag (at it will obviously fail while trying to (re)push the image),
    2. you have to deactivate publish on `master` branch by setting the `$PUBLISH_ON_PROD` variable to `false` (as it would lead to the `master` tag being overwritten).

### Registries and credentials

As seen in the previous chapter, the CNB template uses by default the GitLab registry to push snapshot and release images.
Thus it makes use of credentials provided by GitLab itself to login (`CI_REGISTRY_USER` / `CI_REGISTRY_PASSWORD`).

But when using other registry(ies), you'll have also to **configure appropriate credentials**.

#### Using the same registry for snapshot and release

If you use the **same registry** for both snapshot and release images, you shall use the following configuration
variables:

| Name                             | Description                            |
| -------------------------------- | -------------------------------------- |
| :lock: `CNB_REGISTRY_USER`    | container registry username for image registry |
| :lock: `CNB_REGISTRY_PASSWORD`| container registry password for image registry  |

#### Using different registries for snapshot and release

If you use **different registries** for snapshot and release images, you shall use separate configuration variables:

| Name                                     | Description                            |
| ---------------------------------------- | -------------------------------------- |
| :lock: `CNB_REGISTRY_SNAPSHOT_USER`   | container registry username for snapshot image registry |
| :lock: `CNB_REGISTRY_SNAPSHOT_PASSWORD`| container registry password for snapshot image registry |
| :lock: `CNB_REGISTRY_RELEASE_USER`    | container registry username for release image registry |
| :lock: `CNB_REGISTRY_RELEASE_PASSWORD`| container registry password for release image registry |

### Secrets management

Here are some advices about your **secrets** (variables marked with a :lock:):

1. Manage them as [project or group CI/CD variables](https://docs.gitlab.com/ee/ci/variables/#add-a-cicd-variable-to-a-project):
    * [**masked**](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) to prevent them from being inadvertently
      displayed in your job logs,
    * [**protected**](https://docs.gitlab.com/ee/ci/variables/#protected-cicd-variables) if you want to secure some secrets
      you don't want everyone in the project to have access to (for instance production secrets).
2. In case a secret contains [characters that prevent it from being masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable),
  simply define its value as the [Base64](https://en.wikipedia.org/wiki/Base64) encoded value prefixed with `@b64@`:
  it will then be possible to mask it and the template will automatically decode it prior to using it.
3. Don't forget to escape special characters (ex: `$` -> `$$`).

## Jobs

### `cnb-build` job

This job builds the image and publishes it to the _snapshot_ repository.

It is bound to the `package-build` stage, and uses the following variables:

It uses the following variable:

| Name                  | description                              | default value     |
| --------------------- | ---------------------------------------- | ----------------- |
| `CNB_APP_DIR`        | Relative path to the application source code base directory in your repository | `.` |

<!--
In addition to the above variables, the CNB template also supports all configuration means supported
by the builder and buildpacks you're using.

If you're using Packeto Builder and Buildpacks (default), you shall use:

* Builder [environment variables](https://github.com/buildpacks/rfcs/blob/main/text/0026-lifecycle-all.md#usage) (ex: `CNB_USER_ID`, `CNB_GROUP_ID`, ...),
* Buildpacks [configuration mechanisms](https://paketo.io/docs/howto/configuration/#types-of-configuration):
    * [Environment Variables](https://paketo.io/docs/howto/configuration/#environment-variables) - used for generic configuration at both **build-time** and **runtime**.
    * [`buildpack.yml`](https://paketo.io/docs/howto/configuration/#buildpackyml) - used for generic configuration at **build-time**.
    * [Bindings](https://paketo.io/docs/howto/configuration/#bindings) - used for **secret** configuration at both **build-time** and **runtime**.
    * [Procfiles](https://paketo.io/docs/howto/configuration/#procfiles) - used to provide custom **process types** at **build-time**.

If you selected an alternate Builder and Buildpacks (by overriding the `CNB_BUILDER_IMAGE` variable), then refer to the related documentation.
-->

This job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

| Name               | Description                                            | Example                                 |
| ------------------ | ------------------------------------------------------ | --------------------------------------- |
| `cnb_image`        | snapshot image name **with tag**                       | `registry.gitlab.com/acme/website/snapshot:main` |
| `cnb_image_digest` | snapshot image name **with digest** (no tag)           | `registry.gitlab.com/acme/website/snapshot@sha256:b7914a91...` |
| `cnb_repository`   | snapshot image **bare repository** (no tag nor digest) | `registry.gitlab.com/acme/website/snapshot`      |
| `cnb_tag`          | snapshot image tag                                     | `main`                                  |
| `cnb_digest`       | snapshot image digest                                  | `sha256:b7914a91...`                    |

They may be freely used in downstream jobs (for instance to deploy the upstream built image, whatever the branch or tag).

#### User-Provided variables support

> [User-Provided variables](https://github.com/buildpacks/spec/blob/main/platform.md#user-provided-variables) must be prefixed with `CNB_X_`
>
> :information_source: _User-Provided variables are variables passed with the `--env` option using `pack`_

Examples or User-Provided variables:

* [`BP_PHP_WEB_DIR`](https://github.com/paketo-buildpacks/php-builtin-server#bp_php_web_dir) (supported by the Paketo PHP buildpack) must be declared as `CNB_X_BP_PHP_WEB_DIR`,
* [`BP_KEEP_FILES`](https://github.com/paketo-buildpacks/go-build#bp_keep_files) (supported by the Paketo Go buildpack) must be declared as `CNB_X_BP_KEEP_FILES`,
* [`MAVEN_SETTINGS_PATH`](https://devcenter.heroku.com/articles/using-a-custom-maven-settings-xml#defining-the-maven_settings_path-config-variable) (supported by the Heroku Maven buildpack) must be declared as `CNB_X_MAVEN_SETTINGS_PATH`,
* [`GOOGLE_GOLDFLAGS`](https://github.com/GoogleCloudPlatform/buildpacks#go-buildpacks) (supported by the Google Go buildpack) must be declared as `CNB_X_GOOGLE_GOLDFLAGS`.

### `docker-publish` job

This job pushes (_promotes_) the built image as the _release_ image using [skopeo](https://github.com/containers/skopeo).

| Name                  | Description                                                                 | Default value     |
| --------------------- | --------------------------------------------------------------------------- | ----------------- |
| `CNB_SKOPEO_IMAGE`    | The Docker image used to run [skopeo](https://github.com/containers/skopeo) | `quay.io/skopeo/stable:latest` |
| `CNB_PUBLISH_ARGS`    | Additional [`skopeo copy` arguments](https://github.com/containers/skopeo/blob/master/docs/skopeo-copy.1.md#options) | _(none)_          |
| `AUTODEPLOY_TO_PROD`  | Set to enable automatic publish (and deploy) on `master` branch             | _none_ (enabled)  |
| `PUBLISH_ON_PROD`     | Determines whether this job is enabled on `master` branch                   | `true`_ (enabled)  |

This job produces _output variables_ that are propagated to downstream jobs (using [dotenv artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#artifactsreportsdotenv)):

| Name               | Description                                           | Example                                 |
| ------------------ | ----------------------------------------------------- | --------------------------------------- |
| `cnb_image`        | release image name **with tag**                       | `registry.gitlab.com/acme/website:main` |
| `cnb_image_digest` | release image name **with digest** (no tag)           | `registry.gitlab.com/acme/website@sha256:b7914a91...` |
| `cnb_repository`   | release image **bare repository** (no tag nor digest) | `registry.gitlab.com/acme/website`      |
| `cnb_tag`          | release image tag                                     | `main`                                  |
| `cnb_digest`       | release image digest                                  | `sha256:b7914a91...`                    |

They may be freely used in downstream jobs (for instance to deploy the upstream built image, whatever the branch or tag).
