## [1.0.2](https://gitlab.com/to-be-continuous/cnb/compare/1.0.1...1.0.2) (2023-01-27)


### Bug Fixes

* "Add registry name in all Docker images" ([14fba12](https://gitlab.com/to-be-continuous/cnb/commit/14fba124571a09ca22a84e1fd25d522f3e2462b3))

## [1.0.1](https://gitlab.com/to-be-continuous/cnb/compare/1.0.0...1.0.1) (2022-09-28)


### Bug Fixes

* correct split of cnb_image when registry port is present ([bfe7372](https://gitlab.com/to-be-continuous/cnb/commit/bfe7372f8c1f50b494a6a31874099ef13e4e18fe))

# 1.0.0 (2022-08-31)


### Features

* initial release ([6628608](https://gitlab.com/to-be-continuous/cnb/commit/6628608431e5e7e06b3ea7b6a6b1fa45953c9907))
